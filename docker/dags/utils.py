#!/usr/local/bin/python3

from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.sensors.python_sensor import PythonSensor
import os


def append_file(path, line):
    if not line.endswith('\n'):
        line = line + '\n'
    with open(path, "a+") as file_object:
        file_object.write(line)


def count_line(path):
    count = 0
    try:
        with open(path, 'r+') as f:
            for line in f:
                count += 1
    except:
        pass
    return count
