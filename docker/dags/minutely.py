#!/usr/local/bin/python3


from utils import append_file, count_line
from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.sensors.python_sensor import PythonSensor
import os
DIR = os.path.dirname(os.path.realpath(__file__))
MINUTELY_FILE = DIR + '/minutely.txt'
HOURLY_FILE = DIR + '/hourly.txt'


def run_minutely():
    global MINUTELY_FILE
    append_file(MINUTELY_FILE, 'hello world')
    return 'Hello world!'


# def init_minutely():
dag = DAG('inspector-minutely', description='Simple tutorial DAG',
          schedule_interval='*/1 * * * *',
          start_date=datetime(2017, 3, 20), catchup=False)

dummy_operator = DummyOperator(task_id='dummy_task', retries=3, dag=dag)

minutely_operator = PythonOperator(
    task_id='minutely_task', python_callable=run_minutely, dag=dag)

dummy_operator >> minutely_operator
