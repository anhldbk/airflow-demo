#!/usr/local/bin/python3

from utils import append_file, count_line
from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.sensors.python_sensor import PythonSensor
import os
DIR = os.path.dirname(os.path.realpath(__file__))
MINUTELY_FILE = DIR + '/minutely.txt'
HOURLY_FILE = DIR + '/hourly.txt'


def sense():
    lines = count_line(MINUTELY_FILE)
    return lines != 0 and lines % 5 == 0


def run_hourly():
    global HOURLY_FILE
    append_file(HOURLY_FILE, 'bonjour')
    return 'Hello world!'


dag = DAG('inspector-hourly', description='Simple tutorial DAG',
          schedule_interval='*/5 * * * *',
          start_date=datetime(2017, 3, 20), catchup=False)

sense_operator = PythonSensor(
    task_id='sense', python_callable=sense, retries=3, dag=dag)

hourly_operator = PythonOperator(
    task_id='hourly_task', python_callable=run_hourly, dag=dag)

sense_operator >> hourly_operator
