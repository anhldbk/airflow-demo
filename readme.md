# AirFlow

## Overview

[Get started developing workflows with Apache Airflow](http://michal.karzynski.pl/blog/2017/03/19/developing-workflows-with-apache-airflow/)

We should use Docker: [Getting Started with Airflow Using Docker](https://towardsdatascience.com/getting-started-with-airflow-using-docker-cd8b44dbff98)

## Quick start 

You must have Docker installed first

```bash
$ docker/run.sh
#
```

The DAGs predefined are stored in `docker/dags`

- `inspector-minutely`: every 1 minute, it will append a new line into a file 
- `inspector-hourly`: every 5 minutes, it will read from that file and perform some batching magic


## Knowledge

### Installation

```sh
$ pip3 install apache-airflow
#
$ airflow initdb
#
```

### Prepare

Follow [this tutorial](https://airflow.apache.org/docs/stable/tutorial.html)

### Run your DAGs

Let’s assume we’re saving the code from the previous step in tutorial.py in the DAGs folder referenced in your airflow.cfg. The default location for your DAGs is ~/airflow/dags.

```sh
$ python ~/airflow/dags/tutorial.py

# print the list of active DAGs
$ airflow list_dags

# prints the list of tasks the "tutorial" dag_id
$ airflow list_tasks tutorial

# prints the hierarchy of tasks in the tutorial DAG
$ airflow list_tasks tutorial --tree
```

### Testing

```sh
# command layout: command subcommand dag_id task_id date

# testing print_date
$ airflow test tutorial print_date 2015-06-01

# testing sleep
$ airflow test tutorial sleep 2015-06-01
```

`Note`: that the `airflow test` command runs task instances locally, outputs their log to stdout (on screen), doesn’t bother with dependencies, and doesn’t communicate state (running, success, failed, …) to the database. It simply allows testing a single task instance.

### Backfill

There can be the case when you may want to run the dag for a specified historical period e.g., A data filling DAG is created with start_date 2019-11-21, but another user requires the output data from a month ago i.e., 2019-10-21. This process is known as Backfill.

`backfill` will respect your dependencies, emit logs into files and talk to the database to record status. 

If you do have a webserver up, you’ll be able to track the progress. `airflow webserver` will start a web server if you are interested in tracking the progress visually as your backfill progresses.

```sh
$ airflow webserver --debug &

# start your backfill on a date range
$ airflow backfill tutorial -s 2015-06-01 -e 2015-06-07
#
```

### Tailing the logs

Start the scheduler and trigger a dag.

```sh
$ airflow scheduler
#
$ airflow trigger_dag my-dag
#
```

Watch the output with tail -f.

```sh
$ tail -f ~/airflow/logs/my-dag.log
```

## Resources


[Apache Airflow: Tutorial and Beginners Guide](https://www.polidea.com/blog/apache-airflow-tutorial-and-beginners-guide/)
